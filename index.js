const AWS = require("aws-sdk");
const request = require("request");

const ffmpeg = require("ffmpeg");
const sharp = require("sharp");
const fs = require("fs");
const path = require("path");
const stream = require("stream");
const AWSS3 = require("@aws-sdk/client-s3");

module.exports = class MediaService {
  constructor(uploadPath = "/uploads") {
    AWS.config.update({
      region: process.env.AWS_REGION,
      signatureVersion: "v4",
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    });
    this.awsS3 = new AWS.S3({ apiVersion: "2006-03-01" });
    this.Bucket = process.env.AWS_S3_BUCKET || "legiski-dev";
    this.defaultPath = uploadPath;
    this.ffmpeg = ffmpeg;
    this.sharp = sharp;
    this.fs = require("fs");
    this.path = path;
    this.stream = stream;
    this.imgHeight = 300;
    this.imgWidth = 500;
    this.imgFormat = "jpg";
    this.imgQuality = 80;
    this.imgSizes = {
      large: { w: 900 },
      medium: { w: 500 },
      small: { w: 200 },
      thumb: { w: 120 },
      facebook: { h: 600 },
    };
  }

  getLoFolder() {}

  /**
   * Upload images to AWS S3 in many different format sizes.
   * @param {string=} imagePath path of the image to upload
   * @param {object=} options option can contain sizes, aws bucket, image quality.
   * @example  of param options : {
   *              sizes: { large: { w: 900, h: 500 }, facebook: { h: 600 } },
   *              bucket: 'My bucket',
   *              quality: 80,
   *              format: 'jpeg',
   *              subFolder: 'users'
   *          }
   *
   * @throws — {Error} Invalid parameters
   * @return an array's string of links of uploaded images
   */
  async uploadImageToAWSS3WithDifferentSizes(imagePath, options = null) {
    if (options.sizes) this.imgSizes = options.sizes;
    if (options.bucket) this.Bucket = options.bucket;
    if (options.quality) this.imgQuality = options.quality;
    if (options.format) this.imgFormat = options.format;

    try {
      const userId = 687;
      const fileContent = await this.getFile(imagePath);

      const uploads = Object.keys(this.imgSizes).map((imgFormat) => {
        const key = `users/${userId}/${imgFormat}.jpeg`;
        return new Promise(async (resolve, reject) => {
          const file = await this.sharp(fileContent)
            .resize(this.imgSizes[imgFormat].w, this.imgSizes[imgFormat].h)
            [this.imgFormat]({ mozjpeg: true })
            .toBuffer();

          if (!file) return reject("error with sharp : ", file);

          const finalFile = await this.streamToS3(file, key);

          if (!finalFile) return reject("Error when uploading file to S3 : ", finalFile);

          return resolve(finalFile);
        });
      });

      return Promise.all(uploads)
        .then((files) => files)
        .catch((err) => err);
    } catch (error) {
      throw new Error("Error occurred when processing ", error);
    }
  }

  async streamToS3(buffer, key) {
    return new Promise((resolve, reject) => {
      let dataStream = stream.Readable();
      dataStream._read = () => {};
      dataStream.push(buffer);
      dataStream.push(null);
      dataStream.pause();

      return this.awsS3
        .upload({
          Bucket: this.Bucket,
          Key: key,
          ContentType: "image/jpeg",
          Body: dataStream,
        })
        .on("httpUploadProgress", (progress) => console.log(progress))
        .send((err, resp) => {
          if (err) reject(err);
          return resolve(resp);
        });
    });
  }

  genLink(key, Bucket) {
    return `//${this.awsS3.endpoint.hostname}/${Bucket}/${key}`;
  }

  async readFile(path) {
    try {
      if (!this.fs.existsSync(path)) throw new Error("File does not exists !");
      return this.fs.createReadStream(path);
    } catch (error) {
      return null;
    }
  }

  streamToBuffer(stream) {
    return new Promise((resolve, reject) => {
      const buff = [];
      if (stream) {
        stream.on("data", (chunk) => buff.push(chunk));
        stream.on("end", () => resolve(Buffer.concat(buff)));
        stream.on("error", (err) => reject(err));
      }
    });
  }

  async getFile(path) {
    return new Promise(async (resolve, reject) => {
      if (path.includes("http:") || path.includes("https:")) {
        request.get(path, { encoding: null }, (err, response, body) => {
          if (err) reject(err);
          console.log("The content of the body downloaded ", body);
          return resolve(body);
        });
      } else {
        const result = await this.readFile(path);
        if (!result) reject(result);
        const buffer = await this.streamToBuffer(result);
        if (!buffer) reject(result);
        resolve(buffer);
      }
    });
  }
};
