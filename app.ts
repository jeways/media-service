interface App {
    name: string
    id: number
    public_key: string
    secret_key: string
    api_url: string[]
    url_website: string
    plan: Plan,
    createdAt: string | Date
    updatedAt: string | Date
    activated: boolean
    suspend: boolean
    suspension_reason: string
    UserId: number
    User: User
}

interface Plan {
    id: number
    name: string,
    frequence: string
    payment: Payment
    amount: number
    cative: boolean
}


interface Payment {
    payment_method: string,
    save: boolean
    card_number: string
    part_card_number: string
}

interface User {
    id: number
    firstName: string
    lastName: string
    displayName: string
    type: string
    active: boolean
    delete: boolean
    phone: string,
    email: string,
    emailverified: boolean
    password: string
}

interface OrderItem {
    id: number
    name: string
    quantity: number
    description?: string
    othersprops?: any
}

interface Order {
    id: number
    code: string
    location: string
    products: OrderItem[]
    receiver : Receiver
    receiver_address: Address
    shippingtype: Shippingtype
}

interface Location {
    id: number
    last_location: string
    current_location: string
}

interface Receiver {
    id: number
    full_name: string
    first_name: string
    last_name: string
    phone: string
    sexe: string
}

interface ShippingEvent {
    id: number
    status: string
    savedLocation: string
    eventdescription: string
    createdAt: string | Date
    updatedAt: string | Date
    shippingProcessId: number

}

interface ShippingProcess {
    id: number
    receiver : Receiver
    receiver_address: Address
    shippingtype: Shippingtype
    address: Address
    createdAt: Date | string
    updatedAt: Date | string
    ShippingEvents: Array<ShippingEvent>
}

interface Address {
    id: number
    longitude: number
    latitude: number
    country: string
    city: string
    region: string
    street: string
    building_number:number
    floor_number:number
    house_number: number
    house_type: string
}

interface MeetPoint {
    id: number
    city: City
    slug_name: string
    detail_name: string
    description?: string
}

enum Shippingtype {
    meetUp = "meetup",
    homeToHome = "hometohome"
}